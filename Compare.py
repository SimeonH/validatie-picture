import os
import nibabel as nib
import numpy as np
import matplotlib.pyplot as plt

def pad_to_match(data1, data2):
    if data1.shape == data2.shape:
        return data1  # Geen padding nodig als ze al gelijk zijn.

    # Bepaal welke data het kleinste is en moet worden gepadded
    smaller_data, larger_data = (data1, data2) if np.prod(data1.shape) < np.prod(data2.shape) else (data2, data1)

    pad_width = []
    for i in range(3):  # Loop over de drie dimensies
        if smaller_data.shape[i] < larger_data.shape[i]:
            diff = larger_data.shape[i] - smaller_data.shape[i]
            pad_before = diff // 2
            pad_after = diff - pad_before
            pad_width.append((pad_before, pad_after))
        else:
            pad_width.append((0, 0))  # Geen padding nodig voor deze dimensie

    padded_data = np.pad(smaller_data, pad_width, mode='constant', constant_values=0)
    
    # Retourneer de juiste gepadde data afhankelijk van welke data kleiner was.
    return padded_data if smaller_data is data1 else data1

def dice_coefficient(y_true, y_pred):
    intersection = np.sum(y_true * y_pred)
    return (2. * intersection) / (np.sum(y_true) + np.sum(y_pred))

def jaccard_index(y_true, y_pred):
    intersection = np.sum(y_true * y_pred)
    union = np.sum(y_true) + np.sum(y_pred) - intersection
    return intersection / union

def compute_confusion_matrix(y_true, y_pred):
    TP = np.sum((y_true == 1) & (y_pred == 1))
    TN = np.sum((y_true == 0) & (y_pred == 0))
    FP = np.sum((y_true == 0) & (y_pred == 1))
    FN = np.sum((y_true == 1) & (y_pred == 0))
    return TP, TN, FP, FN

def compute_metrics(TP, TN, FP, FN):
    accuracy = (TP + TN) / (TP + TN + FP + FN)
    sensitivity = TP / (TP + FN) if (TP + FN) > 0 else 0
    specificity = TN / (TN + FP) if (TN + FP) > 0 else 0
    return accuracy, sensitivity, specificity

def analyze_slices(data, data2, slice_index, ai_file, expert_file, t1_data, fn_slices):
    dice_score = dice_coefficient(data2, data)
    jaccard_score = jaccard_index(data2, data)
    TP, TN, FP, FN = compute_confusion_matrix(data2, data)
    accuracy, sensitivity, specificity = compute_metrics(TP, TN, FP, FN)
    
    print(f"Slice {slice_index} - DICE Score: {dice_score:.4f}, Jaccard Index: {jaccard_score:.4f}")
    print(f"Confusion Matrix: TP={TP}, TN={TN}, FP={FP}, FN={FN}")
    print(f"Metrics: Accuracy={accuracy:.4f}, Sensitivity={sensitivity:.4f}, Specificity={specificity:.4f}")
    
    combined_segmentation = np.zeros(data.shape, dtype=int)
    combined_segmentation[data == 1] = 1  
    combined_segmentation[data2 == 1] = 2  
    
    FN_region = np.logical_and(data == 0, data2 == 1)  
    FN_region = np.flip(FN_region, axis=1)
    FN_region = np.flip(FN_region, axis=0)
    fn_slices[:, :, slice_index] = FN_region.astype(int)  # Doe de FN regio's voor alle slices
    
    plt.figure(figsize=(24, 6))
    plt.suptitle(f"Analyse van {ai_file} en {expert_file}")
    
    plt.subplot(1, 5, 1)
    plt.imshow(t1_data[:, :, slice_index].T, cmap='gray', origin='lower')
    plt.title(f'Slice {slice_index} - T1 MRI')
    plt.colorbar()
    
    plt.subplot(1, 5, 2)
    plt.imshow(data.T, cmap='gray', origin='lower')
    plt.title(f'Slice {slice_index} - AI Segmentatie')
    plt.colorbar()
    
    plt.subplot(1, 5, 3)
    plt.imshow(data2.T, cmap='gray', origin='lower')
    plt.title(f'Slice {slice_index} - Expert Segmentatie')
    plt.colorbar()
    
    plt.subplot(1, 5, 4)
    plt.imshow(FN_region.T, cmap='Reds', origin='lower')
    plt.title(f'Slice {slice_index} - False Negatives')
    plt.colorbar()
    
    plt.subplot(1, 5, 5)
    plt.title(f"Slice {slice_index} - Gecombineerde Segmentatie")
    plt.imshow(combined_segmentation.T, cmap='viridis', origin='lower')
    plt.colorbar(ticks=[0, 1, 2], label='Segmentatie Type')
    plt.clim(0, 2)
    plt.show()
    
def match_and_compare(ai_dir, expert_base_dir):
    ai_files = [f for f in os.listdir(ai_dir) if f.endswith('_segmentation.nii')]
    
    for ai_file in ai_files:
        patient_id = ai_file.split('_segmentation.nii')[0]
        expert_file = patient_id + '_modified_seg.nii.gz'
        t1_file = patient_id + '_t1.nii.gz'
        expert_path = os.path.join(expert_base_dir, patient_id, expert_file)
        t1_path = os.path.join(expert_base_dir, patient_id, t1_file)
        
        if os.path.exists(expert_path):
            print(f"Analyse van {ai_file} en {expert_file}")
            img = nib.load(os.path.join(ai_dir, ai_file))
            data = img.get_fdata()
            data = np.flip(data, axis=0)
            
            img2 = nib.load(expert_path)
            data2 = img2.get_fdata()
            data2 = np.flip(data2, axis=1)
            data2 = np.flip(data2, axis=0)
            
            t1_img = nib.load(t1_path)
            t1_data = t1_img.get_fdata() 
            t1_data = np.flip(t1_data, axis=0)
            t1_data = np.flip(t1_data, axis=1)
            
            if data.shape != data2.shape:
                if np.prod(data.shape) < np.prod(data2.shape):
                    data = pad_to_match(data, data2)
                else:
                    data2 = pad_to_match(data2, data)
            
            num_slices = min(data.shape[2], data2.shape[2])
            total_dice_scores = []
            total_jaccard_scores = []
            total_TPs, total_TNs, total_FPs, total_FNs = 0, 0, 0, 0
            fn_slices = np.zeros_like(data2) 
            
            for slice_index in range(num_slices):
                slice_data = data[:, :, slice_index]
                slice_data2 = data2[:, :, slice_index]
                
                TP, TN, FP, FN = compute_confusion_matrix(slice_data2, slice_data)
                
                if TP == 0:  # slices met 0 TP kunnen worden overgeslagen
                    continue
                
                dice_score = dice_coefficient(slice_data2, slice_data)
                jaccard_score = jaccard_index(slice_data2, slice_data)
                
                total_dice_scores.append(dice_score)
                total_jaccard_scores.append(jaccard_score)
                total_TPs += TP
                total_TNs += TN
                total_FPs += FP
                total_FNs += FN
                
                analyze_slices(slice_data, slice_data2, slice_index, ai_file, expert_file, t1_data, fn_slices)

            output_path = os.path.join(ai_dir, 'FN_masks')
            if not os.path.exists(output_path):
                os.makedirs(output_path)
            output_fn_file = os.path.join(output_path, f'{patient_id}_fn.nii')
            if not os.path.exists(output_fn_file):
                fn_img = nib.Nifti1Image(fn_slices.astype(np.int16), img.affine)  
                fn_img.to_filename(output_fn_file)
                print(f"FN NIFTI file saved for {patient_id}")
            else:
                print(f"FN file for {patient_id} already exists, not overwriting.")       
            
            if total_dice_scores:  
                average_dice_score = np.mean(total_dice_scores)
                average_jaccard_score = np.mean(total_jaccard_scores)
                
                accuracy, sensitivity, specificity = compute_metrics(total_TPs, total_TNs, total_FPs, total_FNs)
                
                print(f"Average DICE Score: {average_dice_score:.4f}, Average Jaccard Index: {average_jaccard_score:.4f}")
                print(f"Total Confusion Matrix: TP={total_TPs}, TN={total_TNs}, FP={total_FPs}, FN={total_FNs}")
                print(f"Metrics: Accuracy={accuracy:.4f}, Sensitivity={sensitivity:.4f}, Specificity={specificity:.4f}")
                print("----------------------------------------------------------------------")
                
        else:
            print(f"No matching expert segmentation found for {patient_id}")

ai_dir = '/Users/simeonhailemariam/Documents'
expert_base_dir = '/Users/simeonhailemariam/Downloads/archive/BraTS2021_Training_Data'
match_and_compare(ai_dir, expert_base_dir)
